import React, { FunctionComponent, useState } from "react";
import "./App.css";

type Meal = { key: number; name: string };

const data: Meal[] = [
  { key: 1, name: "Mac Cheese" },
  { key: 2, name: "Roast chicken" },
  { key: 3, name: "Fajita" },
  { key: 4, name: "Spicy Thai Peanut Sauce over Roasted Sweet Potatoes" },
  { key: 5, name: "Pizza" },
  { key: 6, name: "Prawn Masala" },
  { key: 7, name: "Beans and rice" },
  { key: 8, name: "Sugar Snap Pea and Carrot Soba Noodles" },
  { key: 9, name: "Eggs on Toast" },
];

// Challenge:
// Present the user with a list of meal suggestions.
// For each suggestion, the user can click a button labelled "Pick" or "Reject"
// Clicking the "Pick" button makes the selected meal appear in the list of "Chosen meals"
// Clicking the "Reject" button makes the selected meal appear in the list of "Rejected meals"
// After clicking either "Pick" or "Reject", the selected meal should be removed from the list of "Remaining suggestions"
//
// This App component provides the basic template for this functionality.
// Your challenge is to add click handlers to the "Pick" and "Reject" buttons
// and to compute the contents of the three lists:
//
// * remainingSuggestions
// * chosenMeals
// * rejectedMeals
//
// Good luck!

const App: FunctionComponent = () => {

  const allMeals = data;
  const remainingSuggestions = allMeals;
  const chosenMeals: Meal[] = [];
  const rejectedMeals: Meal[] = [];

  const [state, setState] = useState({
    remaining: remainingSuggestions,
    chosen: chosenMeals,
    rejected: rejectedMeals
  });

  const setMealState = (remaining: Meal[], chosen: Meal[], rejected: Meal[] )=>{
    setState({
      remaining: remaining,
      chosen: chosen,
      rejected: rejected
    })
  }

  const getSelectedMeal = (meal: Meal) => {
    return allMeals.filter(m => m.key === meal.key);
  }

  const pickOrRejectMeal = (meal: Meal, action: string) => {

    if (meal && meal.key){
      const selectedMeal: Meal[] = getSelectedMeal(meal);
      const allRemaining = state.remaining;
      
      allRemaining.splice(remainingSuggestions.indexOf(selectedMeal[0]),1);

      if ( action === "PICK"){
        const allChosen: Meal[] = state.chosen;
        allChosen.push(selectedMeal[0]);
        setMealState(allRemaining,allChosen, state.rejected);
      } else if( action === "REJECT") {

        const allRejected: Meal[] = state.rejected;
        allRejected.push(selectedMeal[0]);
        setMealState(allRemaining,state.chosen, allRejected);
      }
    }
  }

  const pickMeal = (meal: Meal) => {
    pickOrRejectMeal(meal, "PICK");
  }
  
  const rejectMeal = (meal: Meal) => {
    pickOrRejectMeal(meal, "REJECT");
  }

  return (
    <div>
      <h1>Meal Planner</h1>

      <h2>Remaining suggestions [{state.remaining.length}]</h2>
      <ul>
        {state.remaining.map((meal) => {
          return (
            <li key={meal.key}>
              <span>{meal.name}</span>
              <button onClick={()=>pickMeal(meal)}>Pick</button>
              <button onClick={()=>rejectMeal(meal)}>Reject</button>
            </li>
          );
        })}
      </ul>

      <h2>Chosen meals [{state.chosen.length}]</h2>
      <ul>
        {state.chosen.map((meal) => {
          return <li key={meal.key}>{meal.name}</li>;
        })}
      </ul>

      <h2>Rejected meals [{state.rejected.length}]</h2>
      <ul>
        {state.rejected.map((meal) => {
          return <li key={meal.key}>{meal.name}</li>;
        })}
      </ul>
    </div>
  );
};

export default App;
